Pour créeer des fixtures:

'composer require --dev orm-fixtures' qui nous crée un dossier DataFixtures
'symfony console make:fixtures' puis on nomme avec le nom de la classe qu'on veut implémenter
Ensuite dans le fichier crée on set les données puis persist et flush pour avoir nos données en BDD
et on tape la commande 'symfony console doctrine:fixtures:load'

Pour utiliser VichUploader:

'composer require vich/uploader-bundle'
paramétrage su fichier vich_uploader.yaml en décommentant ce qui est fourni puis en renseignant le bon mapping.
Dans notee classe, renseigner: 
- 'use Vich\UploaderBundle\Entity\File;'
- 'use Vich\UploaderBundle\Mapping\Annotation as Vich;'
ajouter '* @Vich\Uploadable' dans l'annotation au dessus de la classe
ajouter les getters et setters de la propriété $imageFile dans la classe correspondante qu'on peut trouver sur la doc 

Pour utiliser liipimaginebundle:

'composer require liip/imagine-bundle'