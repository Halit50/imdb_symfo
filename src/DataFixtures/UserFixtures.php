<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $this->encoder = $userPasswordHasherInterface;
    }
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $user = new User;
        $user->setEmail('admin@gmail.com');
        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
        $plainPassword = "pass";
        $pass = $this->encoder->hashPassword($user, $plainPassword);
        $user->setPassword($pass);
        $manager->persist($user);

        $user = new User;
        $user->setEmail('user@gmail.com');
        $user->setRoles(['ROLE_USER']);
        $plainPassword = "pass";
        $pass = $this->encoder->hashPassword($user, $plainPassword);
        $user->setPassword($pass);
        $manager->persist($user);

        $manager->flush();
    }

    public static function getGroups() : array
    {
        return['user'];
    }
}
