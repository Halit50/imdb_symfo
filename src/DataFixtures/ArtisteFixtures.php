<?php

namespace App\DataFixtures;

use App\Entity\Artiste;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ArtisteFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $artiste = new Artiste();
        $artiste->setLastname('Eastwood');
        $artiste->setFirstname('Clint');
        $artiste->setBirthdate(new DateTime('31-12-1930'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('clint.jpg');
        $manager->persist($artiste);

        $artiste = new Artiste();
        $artiste->setLastname('Cooper');
        $artiste->setFirstname('Bradley');
        $artiste->setBirthdate(new DateTime('15-01-1975'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('cooper.jpg');
        $manager->persist($artiste);

        $artiste = new Artiste();
        $artiste->setLastname('Miller');
        $artiste->setFirstname('Sienna');
        $artiste->setBirthdate(new DateTime('02-02-1980'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('sienna.jpg');
        $manager->persist($artiste);

        $artiste = new Artiste();
        $artiste->setLastname('Ford');
        $artiste->setFirstname('Harrison');
        $artiste->setBirthdate(new DateTime('13-07-1942'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('ford.jpg');
        $manager->persist($artiste);

        $artiste = new Artiste();
        $artiste->setLastname('Hamill');
        $artiste->setFirstname('Mark');
        $artiste->setBirthdate(new DateTime('05-09-1951'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('hamill.jpg');
        $manager->persist($artiste);

        $artiste = new Artiste();
        $artiste->setLastname('Spielberg');
        $artiste->setFirstname('Steven');
        $artiste->setBirthdate(new DateTime('18-05-1946'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('spielberg.jpg');
        $manager->persist($artiste);

        $artiste = new Artiste();
        $artiste->setLastname('Lucas');
        $artiste->setFirstname('George');
        $artiste->setBirthdate(new DateTime('23-10-1945'));
        $artiste->setUpdateAt(new DateTime('now'));
        $artiste->setImagename('lucas.jpg');
        $manager->persist($artiste);

        $manager->flush();
    }
}
